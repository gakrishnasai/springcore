package com.pack;
//@author Krishnasai GA

import org.springframework.stereotype.Component;

@Component("idTutorial")
public class Math implements Tutorial {

	@Override
	public String getDailyTutorial() {
		return "Math Tutorial";
	}
}
