package com.pack;

import org.springframework.context.support.ClassPathXmlApplicationContext;

//@author Krishnasai GA
public class School {

	public static void main(String[] args) {
		
		//Read Spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationcontext.xml");
		
		//Get Bean from Spring container
		Tutorial whatTutorial = context.getBean("idTutorial", Tutorial.class);
		
		//Call Method from Bean
		System.out.println(whatTutorial.getDailyTutorial());
		
		//Close context
		context.close();
	}
}
