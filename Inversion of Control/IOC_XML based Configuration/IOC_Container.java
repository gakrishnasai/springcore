package com.pack;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//@author Krishnasai GA
public class IOC_Container {
	public static void main(String[] args) {
		
		//load spring configuration file
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationcontext.xml");
		
		//retrieve bean
		Tutorial whatTutorial = context.getBean("idTutorial", Tutorial.class);
		
		//call bean method
		System.out.println(whatTutorial.getDailyTutorial());
		
		//close context
		context.close();
	}
}
