package com.pack;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

//@author Krishnasai GA
public class IOC_Container {
	public static void main(String[] args) {
		
		//load spring configuration file
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(School.class);
		
		//retrieve bean
		Tutorial whatTutorial = context.getBean("physics", Tutorial.class);
		
		//call bean method
		System.out.println(whatTutorial.getDailyTutorial());
		
		//close context
		context.close();
	}
}
