package com.pack;

import org.springframework.stereotype.Component;

//@author Krishnasai GA

@Component
public class Physics implements Tutorial {

	@Override
	public String getDailyTutorial() {
		return "Physics Tutorial";
	}
}
