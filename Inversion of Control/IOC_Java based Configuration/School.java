package com.pack;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

//@author Krishnasai GA
@Configuration
@ComponentScan("com.pack")
public class School {

}
